<?php

namespace ATM\PromotoolsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use \DateTime;

class PromotoolsController extends Controller
{
    /**
     * @Route("/dump" , name="atm_promotools_dump")
     */
    public function indexAction()
    {
        $config = $this->getParameter('atm_promotools_config');
        $em = $this->getDoctrine()->getManager('promotools');

        $request = $this->get('request_stack')->getCurrentRequest();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $dump = null;
        if($request->getMethod() == 'POST'){

            $fields = $request->get('slcFields');
            $separator = (empty($request->get('separator'))) ? ';': $request->get('separator');

            $query = "SELECT a.id,
                             a.title,
                             a.description,
                             a.pornstars,
                             a.tags,
                             a.creation_date,
                             s.name as siteName
                      FROM advertisement a 
                      JOIN site s ON s.id = a.site_id
                      WHERE s.name LIKE '%".$config['site_name']."%'
                      ORDER BY creation_date DESC";

            $statement = $em->getConnection('promotools')->prepare($query);
            $statement->execute();
            $results = $statement->fetchAll();

            $dump = array();
            foreach($results as $result){
                $data = array();
                foreach($fields as $field){
                    if($field == 'url') {
                        $url = 'https://promotools.stiffia.com/';

                        $query = "SELECT COUNT(v.id) as isVideo FROM video v WHERE v.advertisement_id = ".$result['id'];
                        $statement = $em->getConnection('promotools')->prepare($query);
                        $statement->execute();
                        $isVideo = $statement->fetchAll();

                        if($isVideo[0]['isVideo'] == 0){
                            $url.='picture/';
                        }else{
                            $url.='video/';
                        }
                        $data[] = $url . $result['id'].'?affiliateId='.$user->getAffiliateData()->getAffiliateId();
                    }elseif($field != '--'){
                        $data[] = $result[$field];
                    }
                }
                $dump[] = implode($separator,$data);
            }
        }

        return $this->render('ATMPromotoolsBundle:Promotools:dump.html.twig',array(
            'dump' => $dump,
            'separator' => ';'
        ));
    }


}
