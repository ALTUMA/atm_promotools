<?php

namespace ATM\PromotoolsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder
            ->root('atm_promotools')
                ->children()
                    ->scalarNode('post')->isRequired()->end()
                    ->scalarNode('site_name')->isRequired()->end()
                    ->scalarNode('paysite_placeholder')->isRequired()->end()
                ->end();

        return $treeBuilder;
    }
}
